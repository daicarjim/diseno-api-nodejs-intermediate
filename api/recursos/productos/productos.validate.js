const Joi = require('@hapi/joi')
const log = require('../../../utils/logger')

const blueprintProducto = Joi.object({
  titulo: Joi.string().max(100).required(),
  precio: Joi.number().positive().precision(2).required(),
  moneda: Joi.string().length(3).uppercase().required()
})

module.exports = (req, res, next) => {
  let resultado = blueprintProducto.validate(req.body, { abortEarly: false, convert: false })
  if (resultado.error === undefined) {
    next()
  } else {
    let erroresDeValidacion = resultado.error.details.reduce((acumulador, error) => {
      return acumulador + `[${error.message}]`
    }, "")

    log.warn('El siguiente producto no pasó la validación: ', req.body, erroresDeValidacion)
    res.status(400).send(`El producto en el body debe especificar titulo, precio y moneda. Errores en tu request: ${erroresDeValidacion}`)
  }
}